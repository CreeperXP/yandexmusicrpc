import pypresence
from yandex_music import Client
import configparser
client_id = '978995592736944188'

def get_track():
    try:
        queues = client.queues_list()
        last_queue = client.queue(queues[0].id)
        track_id = last_queue.get_current_track()
        track = track_id.fetch_track()
        return track
    except Exception as e:
        return 0


def get_label():
    try:
        track = get_track()
        title = track.title
        return f"{title}"
    except Exception as e:
        return 'Музыка не воспроизводится'


def get_artist():
    try:
        track = get_track()
        artists = ', '.join(track.artists_name())
        return f"{artists}"
    except Exception as e:
        return 'Ждём включение трека'


def get_link():
    try:
        track = get_track()
        return f"https://music.yandex.ru/album/{track['albums'][0]['id']}/track/{track['id']}/"
    except Exception as e:
        return 'https://music.yandex.ru/'


config = configparser.ConfigParser()
config.read('config.ini')
if config.get('token', 'token') == 'None':
    token_ = input("[YandexMusicRPC] - Пожалуйста, введите свой токен, инструкция в файле Readme.txt")
    config.set('token', 'token', token_)
    with open('config.ini', 'w') as f:
        config.write(f)
else:
    print('[YandexMusicRPC] - Токен получен!')
TOKEN = config.get('token', 'token')

client = Client(TOKEN).init()
curr = get_label()

RPC = pypresence.Presence(client_id)
RPC.connect()
RPC.update(
        details=get_label(),
        state=get_artist(),
        large_image='og-image',
        large_text='Яндекс.Музыка',
        buttons=[{'label': 'Сссылка на трек', 'url': get_link()}] if get_link() != '' else [{}]
        )
print(f"[YandexMusicRPC] - RPC обновлён на {get_label()} - {get_artist()}")

while True:
    if get_label() != curr:
        RPC.update(
            details=get_label(),
            state=get_artist(),
            large_image='og-image',
            large_text='Яндекс.Музыка',
            buttons=[{'label': 'Ссылка на трек', 'url': get_link()}] if get_link() != '' else [{}]
        )
        print(f"[YandexMusicRPC] - RPC обновлён на {get_label()} - {get_artist()}")
        curr = get_label()