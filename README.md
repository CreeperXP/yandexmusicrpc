## Yandex Music RPC

Форк [schwarzalexey/yandexmusicrpc](https://github.com/schwarzalexey/yandexmusicrpc) переведённый на русский язык

![discord](./imgs/Discord_gO4VV43TAj.png)

### Инструкция по получению токена

Переходим по [ссылке](https://oauth.yandex.ru/authorize?response_type=token&client_id=23cabbbdc6cd418abb4b39c32c41195d)  
Авторизуемся при необходимости и предоставляем доступ  
Браузер перенаправит на адрес вида `https://music.yandex.ru/#access_token=AQAAAAAYc***&token_type=bearer&expires_in=31535645`  
Очень быстро произойдет редирект на другую страницу, поэтому нужно успеть скопировать ссылку.

![yandex_token](./imgs/token_yandex.png)

Ваш токен, то что находится после `access_token`.